package de.clsky.codingclub.functional.streams.aufgaben.part2.domain;

public enum Fahrzeugklasse {
    LKW, PKW, MOTORRAD
}
