package de.clsky.codingclub.functional.streams.aufgaben.part1;

import de.clsky.codingclub.functional.streams.aufgaben.part1.domain.Fahrzeug;
import de.clsky.codingclub.functional.streams.aufgaben.part1.domain.Parkplatz;
import de.clsky.codingclub.functional.streams.aufgaben.part1.domain.Fahrzeugklasse;

import java.util.Set;

/**
 * Und noch einmal gruppieren nach Fahrzeugklasse und dann die Menge der Fahrzeuge pro Klasse zählen :)
 */
public class Aufgabe7 {
    public static void main(String[] args) {
        var wiesenstr = new Parkplatz("Wiesenstraße", Set.of(
                new Fahrzeug(Fahrzeugklasse.LKW, "MAN", "D-FS 1234"),
                new Fahrzeug(Fahrzeugklasse.LKW, "Iveco", "KS-MC 3235"),
                new Fahrzeug(Fahrzeugklasse.PKW, "VW", "K-HC 1234"),
                new Fahrzeug(Fahrzeugklasse.PKW, "MERCEDES", "DU-AA 123"),
                new Fahrzeug(Fahrzeugklasse.MOTORRAD, "BMW", "I-KS 2021"),
                new Fahrzeug(Fahrzeugklasse.MOTORRAD, "BMW", "D-JS 2311")
        ));
        var elisenstr = new Parkplatz("Elisenstraße", Set.of(
                new Fahrzeug(Fahrzeugklasse.LKW, "Mercedes", "B-BB 889"),
                new Fahrzeug(Fahrzeugklasse.LKW, "Mercedes", "K-WW 3442"),
                new Fahrzeug(Fahrzeugklasse.PKW, "Nissan", "NE-SR 2155"),
                new Fahrzeug(Fahrzeugklasse.PKW, "VW", "NE-CT 4122"),
                new Fahrzeug(Fahrzeugklasse.MOTORRAD, "Suzuki", "K-KS 221")
        ));
        var gruenstr = new Parkplatz("Grünstraße", Set.of(
                new Fahrzeug(Fahrzeugklasse.LKW, "Iveco", "K-AC 2332"),
                new Fahrzeug(Fahrzeugklasse.PKW, "Audi", "D-MM 3784")
        ));
        var alleParkplaetze = Set.of(wiesenstr, elisenstr, gruenstr);
    }
}
