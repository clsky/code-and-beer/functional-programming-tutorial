package de.clsky.codingclub.functional.streams.aufgaben.part2;

import de.clsky.codingclub.functional.streams.aufgaben.part2.domain.Fahrzeug;
import de.clsky.codingclub.functional.streams.aufgaben.part2.domain.KennzeichenBestimmer;
import de.clsky.codingclub.functional.streams.aufgaben.part2.domain.Parkplatz;
import de.clsky.codingclub.functional.streams.aufgaben.part2.domain.Fahrzeugklasse;

import java.util.Optional;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.groupingBy;

/**
 * MasterClass - Das Kennzeichen ist jetzt leider kein String mehr, sondern Optional. Oben versteckt sich ein
 * Fahrzeug, was bereits abgemeldet wurde, sprich, kein Kennzeichen mehr besitzt. Nullst du noch, oder
 * Optionalst du schon? :)
 * <p>
 * Jetzt gehts ans eingemachte!
 * Ich hätte gerne, dass du mir alle Kennzeichen holst und anhand der Kennzeichen die Städte du kennst (Kennzeichenbestimmer)
 * der dort parkenden Fahrzeuge sammelst und zusammenzählst. :) Heavy shit...
 */
public class Aufgabe9 {

    public static void main(String[] args) {
        var wiesenstr = new Parkplatz("Wiesenstraße", Set.of(
                new Fahrzeug(Fahrzeugklasse.LKW, "MAN", "D-FS 1234"),
                new Fahrzeug(Fahrzeugklasse.LKW, "Iveco", "KS-MC 3235"),
                new Fahrzeug(Fahrzeugklasse.PKW, "VW", "K-HC 1234"),
                new Fahrzeug(Fahrzeugklasse.PKW, "MERCEDES", "DU-AA 123"),
                new Fahrzeug(Fahrzeugklasse.MOTORRAD, "BMW", "I-KS 2021"),
                new Fahrzeug(Fahrzeugklasse.MOTORRAD, "BMW", "D-JS 2311")
        ));
        var elisenstr = new Parkplatz("Elisenstraße", Set.of(
                new Fahrzeug(Fahrzeugklasse.LKW, "Mercedes", "B-BB 889"),
                new Fahrzeug(Fahrzeugklasse.LKW, "Mercedes", "K-WW 3442"),
                new Fahrzeug(Fahrzeugklasse.PKW, "Nissan", "NE-SR 2155"),
                new Fahrzeug(Fahrzeugklasse.PKW, "VW", null),
                new Fahrzeug(Fahrzeugklasse.MOTORRAD, "Suzuki", "K-KS 221")
        ));
        var gruenstr = new Parkplatz("Grünstraße", Set.of(
                new Fahrzeug(Fahrzeugklasse.LKW, "Iveco", "K-AC 2332"),
                new Fahrzeug(Fahrzeugklasse.PKW, "Audi", "D-MM 3784")
        ));
        var alleParkplaetze = Set.of(wiesenstr, elisenstr, gruenstr);

        var kennzeichen = alleParkplaetze.stream()
                .flatMap(p -> p.getFahrzeuge().stream())
                .map(Fahrzeug::getKennzeichen)
                .flatMap(Optional::stream)
                .map(KennzeichenBestimmer::bestimmeOrt)
                .flatMap(Optional::stream)
                .collect(groupingBy(Function.identity(), Collectors.counting()));
        kennzeichen.forEach((key, values) -> System.out.println(key + ": " + values));
    }
}
