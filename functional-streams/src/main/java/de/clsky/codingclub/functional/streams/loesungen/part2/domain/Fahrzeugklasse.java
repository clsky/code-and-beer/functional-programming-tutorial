package de.clsky.codingclub.functional.streams.loesungen.part2.domain;

public enum Fahrzeugklasse {
    LKW, PKW, MOTORRAD
}
