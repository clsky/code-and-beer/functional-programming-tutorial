package de.clsky.codingclub.functional.streams.loesungen.part1;

import de.clsky.codingclub.functional.streams.loesungen.part1.domain.Fahrzeug;
import de.clsky.codingclub.functional.streams.loesungen.part1.domain.Parkplatz;
import de.clsky.codingclub.functional.streams.loesungen.part1.domain.Fahrzeugklasse;

import java.util.Set;

import static java.util.stream.Collectors.groupingBy;

public class Aufgabe4 {
    public static void main(String[] args) {
        var parkplatz = new Parkplatz("Wiesenstraße", Set.of(
                new Fahrzeug(Fahrzeugklasse.LKW, "Iveco", "K-AC 2332"),
                new Fahrzeug(Fahrzeugklasse.MOTORRAD, "Suzuki", "K-KS 221"),
                new Fahrzeug(Fahrzeugklasse.LKW, "Mercedes", "K-WW 3442"),
                new Fahrzeug(Fahrzeugklasse.LKW, "Iveco", "KS-MC 3235"),
                new Fahrzeug(Fahrzeugklasse.PKW, "VW", "K-HC 1234"),
                new Fahrzeug(Fahrzeugklasse.PKW, "VW", "NE-CT 4122"),
                new Fahrzeug(Fahrzeugklasse.LKW, "Mercedes", "B-BB 889"),
                new Fahrzeug(Fahrzeugklasse.PKW, "MERCEDES", "DU-AA 123"),
                new Fahrzeug(Fahrzeugklasse.MOTORRAD, "BMW", "I-KS 2021"),
                new Fahrzeug(Fahrzeugklasse.PKW, "Nissan", "NE-SR 2155"),
                new Fahrzeug(Fahrzeugklasse.LKW, "MAN", "D-FS 1234"),
                new Fahrzeug(Fahrzeugklasse.PKW, "Audi", "D-MM 3784"),
                new Fahrzeug(Fahrzeugklasse.MOTORRAD, "BMW", "D-JS 2311")
        ));
        var fahrzeugeByFahrzeugklasse = parkplatz.getFahrzeuge()
                .stream()
                .collect(groupingBy(Fahrzeug::getFahrzeugklasse));
        fahrzeugeByFahrzeugklasse.forEach((key, value) -> System.out.println(key + ": " + value));
    }
}
