package de.clsky.codingclub.functional.streams.aufgaben.part1.domain;

public enum Fahrzeugklasse {
    LKW, PKW, MOTORRAD
}
