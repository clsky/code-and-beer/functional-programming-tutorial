package de.clsky.codingclub.functional.streams.loesungen.part1;

import de.clsky.codingclub.functional.streams.loesungen.part1.domain.Fahrzeug;
import de.clsky.codingclub.functional.streams.loesungen.part1.domain.Parkplatz;
import de.clsky.codingclub.functional.streams.loesungen.part1.domain.Fahrzeugklasse;

import java.util.Set;
import java.util.stream.Collectors;

public class Aufgabe2 {
    public static void main(String[] args) {
        var parkplatz = new Parkplatz("Wiesenstraße", Set.of(
                new Fahrzeug(Fahrzeugklasse.LKW, "MAN", "D-FS 1234"),
                new Fahrzeug(Fahrzeugklasse.PKW, "VW", "K-HC 1234"),
                new Fahrzeug(Fahrzeugklasse.MOTORRAD, "BMW", "I-KS 2021")
        ));
        var kennzeichen = parkplatz.getFahrzeuge()
                .stream()
                .map(Fahrzeug::getKennzeichen)
                .collect(Collectors.toList());
        kennzeichen.forEach(System.out::println);
    }
}
